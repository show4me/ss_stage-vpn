IP_P1=13.41.60.230
IP_P2=13.41.60.230
IP_P3=13.41.60.230

DOCKER_PORT_P1=13000
SERVER_NAMES=("SS-STAGE-1" "SS-STAGE-2" "SS-STAGE-3")

SSH_PORT='2052'
SSH_USER='develop'

SSH_CONNECT_TIMEOUT=5
DOCKER_API_PORT=2375

ENVT=ss-stage
ID=d23HJib67
#LINE FORMAT:  'local_name'  'remote_service:remote_service_port' 'path' 'protocol'

SERVICE_NAMES=(
 "${ENVT}.vpn"       "${ENVT}.vpn:80" '' 'http'
  'grafana'        'ss-grafana:3000' '/' 'http'
  'portainer'      'portainer_portainer:9443' '/' 'https'
  'config'         'ss-config:80' '/config.json' 'http'
  'redis1'         'ss-statistic-001.ddpm88.0001.euw2.cache.amazonaws.com:6379' '/' 'redis'
  'redis2'         'ss-statistic-002.ddpm88.0001.euw2.cache.amazonaws.com:6379' '/' 'redis'
  'redis3'         'main-redis-0001-001.ddpm88.0001.euw2.cache.amazonaws.com:6379' '/' 'redis'
  'redis4'         'main-redis-0001-002.ddpm88.0001.euw2.cache.amazonaws.com:6379' '/' 'redis'
  'aws-mysql'      'database-1.cluster-c6ieltws0wna.eu-west-2.rds.amazonaws.com:3306' '/' 'mysql'
)
